import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ascending',
})
export class AscendingPipe implements PipeTransform {
  transform(subjects: any[]): any[] {
    if (!subjects) return [];
    return subjects?.sort((a, b) =>
      a.subjectName.toLowerCase() < b.subjectName.toLowerCase() ? -1 : 1
    );
  }
}
