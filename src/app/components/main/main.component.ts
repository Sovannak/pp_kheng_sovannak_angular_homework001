import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Subject } from '../sidebar/sidebar.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
})
export class MainComponent {
  subject: Subject = {
    subjectName: 'Angular',
    description: `Angular is an open framework and
  platform for creating Single Page Applications, written in TypeScript and supported and developed by Google.`,
    thumbnail: '../../../assets/images/angular.svg',
  };

  @Input() showMainFromApp: boolean | undefined;
  @Output() showMainToApp: EventEmitter<boolean> = new EventEmitter<boolean>();

  showMainFromAppToFalse(): void {
    this.showMainToApp.emit(!this.showMainFromApp);
  }

  getSubjectFromSideBar(subjectFromSideBar: Subject): void {
    this.subject = subjectFromSideBar;
  }
}
