import { Component, Input } from '@angular/core';
import { Subject } from '../sidebar/sidebar.component';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent {
  @Input() subjectFromMain: Subject | undefined;
}
