import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContentComponent } from './components/content/content.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { MainComponent } from './components/main/main.component';
import { AscendingPipe } from './pipes/ascending.pipe';
import { AuthenticationDirective } from './directives/authentication.directive';

@NgModule({
  declarations: [AppComponent, ContentComponent, SidebarComponent, MainComponent, AscendingPipe, AuthenticationDirective],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
