import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'PP_KHENG_SOVANNAK_ANGULAR_HOMEWORK001';
  showMain: boolean = false;

  getShowMainFromMain(isShow: boolean): void {
    this.showMain = isShow;
  }
}
